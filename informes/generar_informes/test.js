const ObjectsToCsv = require('objects-to-csv');

const data = [
    { periodo : 'Agosto 2018', monto : '$400'},
    { periodo : 'Septiembre 2018', monto : '$300'},
    { periodo : 'Octubre 2018', monto : '$500'},
    { periodo : 'Noviembre 2018', monto : '$500'}
];

let csv = new ObjectsToCsv(data);

(async() => {

    await csv.toDisk('./text.csv');
    console.log(await csv.toString());

})();



process.exit();
