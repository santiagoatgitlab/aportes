const mysql     = require('mysql');
const fs		= require('file-system');

let currentCBIndex  = 0;
let contributors 	= [];

let connection = mysql.createConnection({
    host        : 'localhost',
    user        : 'santiago',
    password    : '',
    database    : 'aportes',
});

connection.connect();

function setNameSpanish(){
    let query = "set lc_time_names = 'es_ES'";
    connection.query(query, function(error, results){
        if (error) throw error;
		getContributors();
    });
}

function getContributors(){
    let query = 'select id, concat(first_name,"_",last_name) as maestro from contributors';
    connection.query(query, function(error, results, fields){
        if (error) throw error;
		contributors = results;
        onContributorsObtained();
    });
}

function startGettingContributions(){
    getContributions(contributors[currentCBIndex].id);
}

function getContributions(contributor_id){
    let query = 'select concat(ucase(left(date_format(period, "%M %Y"),1)),substring(date_format(period, "%M %Y"),2)) as formatted_period ,concat("$",amount) as amount from contributions where contributor_id = ' + contributor_id + 
				' order by period';
    connection.query(query, function(error, results, fields){
        if (error) throw error;
        onContributionsObtained(results);
    });
}

function toFile(contributions){
	console.log(contributors[currentCBIndex].maestro);
	let fileContent = 'Período,Monto\n';
	for (let row of contributions){
		fileContent += row.formatted_period + ',' + row.amount + '\n';
	}
	let fileName = (contributors[currentCBIndex].maestro + '.csv').replace(new RegExp(' ', 'g'), '_');
	fs.writeFile('aportantes/' + fileName , fileContent, function(error){
		if (error) throw error;
		onFileGenerated();
	});
}

//-------------------------------------------------------------------

function onContributorsObtained(){
    if (contributors.length > 0){
        startGettingContributions();
    }
}

function onContributionsObtained(contributions){
	let userContributions = [];
    toFile(contributions);
}

function onFileGenerated(){
    currentCBIndex++;
    if (currentCBIndex < contributors.length){
        getContributions(contributors[currentCBIndex].id);
    }
    else{
		console.log('finished');
		process.exit(0);
    }
}

//-------------------------------------------------------------------

setNameSpanish();
