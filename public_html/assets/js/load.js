$(document).ready(function(){
	/*setTimeout(function(){*/
		/*hideNotifications();*/
	/*},3000);*/
	$('#reception_date').datepicker({
		dateFormat : 'dd/mm/yy'
	});
	if ($('input[type="radio"].month').is(':checked')){
		init_month_frame();
	}
	if ($('input[type="radio"].range').is(':checked')){
		init_range_frame();
	}
	$('input[type="radio"].range').click(function(){
		init_range_frame();
	});
	$('input[type="radio"].month').click(function(){
		init_month_frame();
	});
	$('#contributor_name').change(function(){
		$(this)[0].setCustomValidity("");
		checkContributor();
	});
});
function init_month_frame(){
	$('.frame-range').fadeOut(100, function(){
		$('.frame-month').fadeIn(100);
	});
	$('.frame-month select').attr('required','required');
	$('.frame-range select').removeAttr('required');
	$('#monto_label').html('Monto');
}
function init_range_frame(){
	$('.frame-month').fadeOut(100, function(){
		$('.frame-range').fadeIn(100);
	});
	$('.frame-range select').attr('required','required');
	$('.frame-month select').removeAttr('required');
	$('#monto_label').html('Monto x mes');
}
function hideNotifications(){
	$('.notification.real').fadeOut(300,function(){
		/*$('.notification.real').remove();*/
	})
}
function checkContributor(){
	$contributorName 	= $('#contributor_name').val();
	$contributorId		= "";
	$iterations = 0;
	$('#contributors option').each(function(){
		$currentName = $(this).val();
		if ($currentName == $contributorName){
			$contributorId = $(this).data('id');
			return false;
		}
		$iterations++;
	});
	$('#contributor_id').val($contributorId);
	if ($contributorId == '' || $contributorId == 'undefined'){
		$('#contributor_name')[0].setCustomValidity("Seleccione un nombre de entre las sugerencias");
		return false;
	}
	else{
		$('#contributor_name')[0].setCustomValidity("");
		return true;
	}
}
