$loading_pages = false;
$loaded_pages = 0;
$new_pages_batch = true;

$(document).ready(function(){

	load_pages();

	$('#reception_from, #reception_to, #transference_from, #transference_to').datepicker({
		dateFormat : 'dd/mm/yy'
	});
	
	if ($('#selected-filters .filter').length > 0){
		$('#mother_checkbox').css('display','inline');
		$('#mother_checkbox').click(function(){
			if ($(this).is(':checked')){
				$('.element input[type="checkbox"]').prop('checked',true);
			}
			else{
				$('.element input[type="checkbox"]').prop('checked',false);
			}
		});
	}

	$('.filters-button').click(function(){
		$('#filters').slideToggle(50);
		$('#selected-filters').fadeToggle(50);
	});

	$('#selected-filters .filter span.close').click(function(){
		$filters_url = ""
		$(this).parent().siblings().each(function(){
			$filters_url += '_' + $(this).find('.filter-entry').val();
		});
		$page = $('#page_name').val();
		$basic_url = '/'+$page;
		if ($filters_url != ""){
			$basic_url += '/f';
		}
		window.location.href = $basic_url + $filters_url + '/o_id_desc';
	});

	$('.elements-header span').click(function(){
		$order_direction = "asc";
		if ( $('#order_field').val() == $(this).attr('class') && $('#order_direction').val() == "asc"){
			$order_direction = "desc";
		}
		$url = '/'+$('#page_name').val();
		if ($('#order_url').length > 0){
			$url = $('#order_url').val();
		}
		window.location.href = $url + '/o_' + $(this).attr('class') + '_' + $order_direction;
	});

	$('.filter-action').click(function(){
		$filters_url = "";
	
		if ($('.filter.collector').length > 0 && $('.filter.collector input.user_input').val() != ""){
			$filters_url += '_' + $('.filter.collector input.url_name').val() + "_" + encodeUriComponentProperly( $('.filter.collector input.user_input').val() );
		}

		if ($('.filter.contributor input.user_input').val() != ""){
			$filters_url += '_' + $('.filter.contributor input.url_name').val() + "_" + encodeUriComponentProperly( $('.filter.contributor input.user_input').val() );
		}

		if ($('.filter.amount input.user_input.from').val() != ""){
			$filters_url += '_' + $('.filter.amount input.url_name').val() + "-min_" + $('.filter.amount input.user_input.from').val();
		}

		if ($('.filter.amount input.user_input.to').val() != ""){
			$filters_url += '_' + $('.filter.amount input.url_name').val() + "-max_" + $('.filter.amount input.user_input.to').val();
		}

		if ($('.filter.period select.month.from').val() != "" && $('.filter.period select.year.from').val() != ""){
			$filters_url += '_' + $('.filter.period input.url_name').val() + "-min"
			$filters_url += '_' + $('.filter.period select.year.from').val() + '-' + $('.filter.period select.month.from').val() + '-1';
		}

		if ($('.filter.period select.month.to').val() != "" && $('.filter.period select.year.to').val() != ""){
			$filters_url += '_' + $('.filter.period input.url_name').val() + "-max"
			$filters_url += '_' + $('.filter.period select.year.to').val() + '-' + $('.filter.period select.month.to').val() + '-1';
		}

		if ($('.filter.receptionDate').length > 0 && $('.filter.receptionDate input.user_input.from').val() != ""){
			$filters_url += '_' + $('.filter.receptionDate input.url_name').val() + '-min';
			$splitted_date = $('.filter.receptionDate input.user_input.from').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}

		if ($('.filter.receptionDate').length > 0 && $('.filter.receptionDate input.user_input.to').val() != ""){
			$filters_url += '_' + $('.filter.receptionDate input.url_name').val() + '-max';
			$splitted_date = $('.filter.receptionDate input.user_input.to').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}

		if ($('.filter.transferenceDate input.user_input.from').val() != ""){
			$filters_url += '_' + $('.filter.transferenceDate input.url_name').val() + '-min';
			$splitted_date = $('.filter.transferenceDate input.user_input.from').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}

		if ($('.filter.transferenceDate input.user_input.to').val() != ""){
			$filters_url += '_' + $('.filter.transferenceDate input.url_name').val() + '-max';
			$splitted_date = $('.filter.transferenceDate input.user_input.to').val().split('/');
			$filters_url += '_' + $splitted_date[2] + '-' + $splitted_date[1] + '-' + $splitted_date[0];
		}
	
		if ($('.filter.status').length > 0 && $('.filter.status select').val() != ""){
			$filters_url += '_' + $('.filter.status input.url_name').val() + "_" + $('.filter.status select').val();
		}

		if ($filters_url != ""){
			window.location.href = '/'+$('#page_name').val()+'/f' + $filters_url + '/o_id_desc';
		}

	});

	$('#set_transfered').click(function(){
		setNotification('neutral','Procesando...');
		var $ids = [];
		$('.element input[type="checkbox"]:checked').each(function(){
			$ids.push($(this).parent().parent().find('input.id').val());
		});
		$.ajax({
			url : "/aportes/transferir",
			data : {contribution_ids : $ids},
			method : "post",
			dataType : "json",
			success : function($result){
				if ($result.success){
					$('.element input[type="checkbox"]:checked').each(function(){
						if ($(this).parent().parent().find('div.status.received').length > 0){
							$(this).parent().parent().find('div.status').removeClass('received').addClass('transfered').html('Entregado');
							$(this).parent().parent().find('div.transferenceDate').html($result.date);
							/*$(this).parent().parent().find('input[type="checkbox"]').prop('checked',false);*/
						}
					});
					endRemoteOperation();
					updateStats();
				}
				else{
				}
			}
		})
	});

	$('#set_received').click(function(){
		setNotification('neutral','Procesando...');
		var $ids = [];
		$('.element input[type="checkbox"]:checked').each(function(){
			$ids.push($(this).parent().parent().find('input.id').val());
		});
		$.ajax({
			url : "/aportes/recuperar",
			data : {contribution_ids : $ids},
			method : "post",
			dataType : "json",
			success : function($result){
				if ($result.success){
					$('.element input[type="checkbox"]:checked').each(function(){
						if ($(this).parent().parent().find('div.status.transfered').length > 0){
							$(this).parent().parent().find('div.status').removeClass('transfered').addClass('received').html('Recibido');
							$(this).parent().parent().find('div.transferenceDate').html('-');
							/*$(this).parent().parent().find('input[type="checkbox"]').prop('checked',false);*/
						}
					});
					endRemoteOperation();
					updateStats();
				}
				else{
				}
			}
		})
	});

	$('#set_checked').click(function(){
		setNotification('neutral','Procesando...');
		var $ids = [];
		$('.element input[type="checkbox"]:checked').each(function(){
			$ids.push($(this).parent().parent().find('input.id').val());
		});
		$.ajax({
			url : "/aportes/confirmar",
			data : {contribution_ids : $ids},
			method : "post",
			dataType : "json",
			success : function($result){
				if ($result.success){
					$('.element input[type="checkbox"]:checked').each(function(){
						if ($(this).parent().parent().find('div.status.transfered').length > 0){
							$(this).parent().parent().find('div.status').removeClass('transfered').addClass('checked').html('Confirmado');
							/*$(this).parent().parent().find('input[type="checkbox"]').prop('checked',false);*/
						}
					});
					endRemoteOperation();
					updateStats();
				}
				else{
				}
			}
		})
	});

	$('#set_transfered_back').click(function(){
		setNotification('neutral','Procesando...');
		var $ids = [];
		$('.element input[type="checkbox"]:checked').each(function(){
			$ids.push($(this).parent().parent().find('input.id').val());
		});
		$.ajax({
			url : "/aportes/desconfirmar",
			data : {contribution_ids : $ids},
			method : "post",
			dataType : "json",
			success : function($result){
				if ($result.success){
					$('.element input[type="checkbox"]:checked').each(function(){
						if ($(this).parent().parent().find('div.status.checked').length > 0){
							$(this).parent().parent().find('div.status').removeClass('checked').addClass('transfered').html('Entregado');
							/*$(this).parent().parent().find('input[type="checkbox"]').prop('checked',false);*/
						}
					});
					endRemoteOperation();
					updateStats();
				}
				else{
				}
			}
		})
	});
	
	$('#remove').click(function(){
		$('#remove-confirmation-modal').css('display','flex');
	});

	$('#remove-confirmation-modal button.no').click(function(){
		$('#remove-confirmation-modal').css('display','none');
	});

	$('#remove-confirmation-modal button.yes').click(function(){
		$('#remove-confirmation-modal').css('display','none');
		setNotification('neutral','Procesando: <span class="progress">0</span>%');
		$elementsTotalCount = $('.element input[type="checkbox"]:checked').length;
		$elementsCount = 0;
		$('.element input[type="checkbox"]:checked').each(function(){
			$contribution = $(this).parent().parent();
			$id = $contribution.find('input.id').val();
			$.ajax({
				url : "/aportes/remover",
				data : {id : $id},
				method : "post",
				dataType : "json",
				async: false,
				success : function($result){
					if ($result.success){
						$contribution.remove();
						$elementsCount++;
						$progress = Math.round(100 / $elementsTotalCount * $elementsCount);
						$('.notification.neutral .progress').html($progress);
					}
					else{
					}
				}
			})
		});
		endRemoteOperation();
		updateStats();
		countTotalResults();
	});
	$(window).scroll(function() {
		if(!$loading_pages && $(window).scrollTop() + $(window).height() == $(document).height()) {
		   load_pages();
		}
	});

	$('.elements-header .checkbox input').click(function(){
		if ($(this).prop('checked')){
			$('input[type="checkbox"]').prop('checked',true);
		}
		else{
			$('input[type="checkbox"]').prop('checked',false);
		}
	});

	$(document).click(function(){
		resetTransferenceElements();
	});

});

function load_pages(){
	$loading_pages = true;
	if ($loaded_pages < $('#pages_count').val()){
		$new_pages_batch = true;
		load_next();
	}
}

function load_next(){
	if( ($loaded_pages % 1 != 0 || $new_pages_batch) && $loaded_pages < $('#pages_count').val()){
		$.ajax({
			url : $('#load_url').val() + "/pag_" + (++$loaded_pages),
			method : "get",
			dataType : "json",
			success : function($result){
				if ($result.success){
					$('#contributions').append($result.content);
					onContributionsLoaded();
					load_next();
				}
			}
		})
		$new_pages_batch = false;
	}
	else{
		$loading_pages = false;
		showButtons();
		initCheckboxes();
		showOrHideMainCheckbox();
	}
}

function showButtons(){
	$('.buttons button').hide();
	if ($('.status.received').length > 0){
		$('#set_transfered').show();
		$('#remove').show();
	}
	if ($('.status.transfered').length > 0){
		$('#set_received').show();
		$('#set_checked').show();
	}
	if ($('.status.checked').length > 0){
		$('#set_transfered_back').show();
	}
}

function initCheckboxes(){
	$('.checkbox input').click(function(){
		if ($('.checkbox input:checked').length > 0){
			$('.buttons button').removeAttr('disabled');
		}
		else{
			$('.buttons button').attr('disabled','disabled');
		}
	});
}

function setNotification($type,$content){
	$('.notification.dummy').clone().appendTo('.notifications');
	$('.notifications .notification:last').removeClass('dummy').addClass($type).find('.content').html($content);
}

function endRemoteOperation(){
	showButtons();
	$('.notification.neutral').remove();
	showOrHideMainCheckbox();
}

function onContributionsLoaded(){
	$('.element .transferenceDate').click(function(e){
		e.stopPropagation();
	});
	$('.element .transferenceDate.editable span').click(function(e){
		resetTransferenceElements();
		$parent = $(this).parent();
		$(this).hide();
		$parent.find('input').show();
		$parent.find('input').datepicker({
			dateFormat : 'dd/mm/yy'
		});
		initCurrentTransferenceDatepicker($parent.find('input'));
	});
	updateStats();
}

function updateStats(){
	$count = $('#contributions .element').length;
	$('.shown-results-count').html($count);
	$addition_received 		= 0;
	$addition_transfered 	= 0;
	$('#contributions .element').each(function(){
		if ($(this).find('.status.received').length > 0){
			$amount = parseFloat($(this).find('.amount span').html());
			$addition_received += $amount;
		}
		else if ($(this).find('.status.transfered').length > 0){
			$amount = parseFloat($(this).find('.amount span').html());
			$addition_transfered += $amount;
		}
	});
	$('.results-legend .addition_received').html("$" + addThousandsSeparator($addition_received,"."));
	$('.results-legend .addition_transfered').html("$" + addThousandsSeparator($addition_transfered,"."));

}

function countTotalResults(){

	$url = window.location.href;
	$.ajax({
		url: $url,
		method : 'post',
		dataType : 'json',
		data : { count : true },
		success : function($result){
			$('.total-results-number').html($result.count);
		}
	});
	
}

function showOrHideMainCheckbox(){
	if ($('#contributions .element .checkbox input').length > 0){
		$('#contributions .elements-header .checkbox input').show();
	}
	else{
		$('#contributions .elements-header .checkbox input').hide();
	}
}

function addThousandsSeparator(value,separator){
	return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g,separator);
}

function encodeUriComponentProperly(component){
	component = encodeURIComponent(component)
	return component.replace(/'/g,'%27');
}

function resetTransferenceElements(){
	$('.element .transferenceDate input').hide();
	$('.element .transferenceDate span').show();
}

function initCurrentTransferenceDatepicker($element){
	$element.unbind('change');
	$element.change(function(){
		$id 	= $element.closest('.element').find('.id').val();
		$date 	= $element.val();
		editTransferenceDate($id,$element);
	});
	$element.datepicker("show");
}

function editTransferenceDate($id,$input){
	$.ajax({
		url : "/aportes/editarFecha",
		data : {id : $id, date: $input.val()},
		method : "post",
		dataType : "json",
		success : function($result){
			if ($result.success){
				$input.parent().find('span').html($input.val());
			}
			resetTransferenceElements();
		}
	})
}
