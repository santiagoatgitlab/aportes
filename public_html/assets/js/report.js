$(document).ready(function(){
	$barWidth = $('.graphic-values .bar-width').val();
	$barMarginLeft = $('.graphic-values .bar-margin-left').val();
	$('.graphic .bar').css('width',$barWidth);
	$('.graphic .bar').css('margin-left',$barMarginLeft);
	$('.graphic .bar').each(function(){
		$height = $(this).data('height');	
		$(this).css('height',$height);
	});
});

