<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
	<?php if (isset($email)) { ?>
	<div>
		Se le ha enviado un correo a la dirección <?php echo $email; ?>.</br>
		Siga los pasos indicados en el correo para reestablecer su contraseña
	</div>
	<?php } else if (isset($email_error)) { ?>
	<div>
		<?php echo $email_error; ?>
	</div>
	<?php } else { ?>
	<form id="login-box" method="post" action="">
		<h2>Recuperar contraseña</h2>
		<div>
			<input type="text" placeholder="Email" name="email" value="">
		</div>
			<button title="Enviar el correo para generar una nueva contraseña">Enviar correo</button>
		</div>
	</form>
	<?php } ?>
	<?php if (isset($error) && $error) { ?>
	<div class="error-message inexistant-email">El correo ingresado no existe</div>
	<?php } ?>
	<a class="blue-link" href="/login" title="Volver a la página de Log in">Volver a la página principal</a>
</main>
