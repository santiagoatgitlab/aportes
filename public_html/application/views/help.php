<main id="help">
    <div class="flex">
        <!--Presentación-->
        <div id="tituloayuda">
            <h2>Página de Ayuda</h2>
            <p>A continuación están algunas de las preguntas frecuentes al usar el sistema de aportes.</p>
            <p>Si ves algo para agregar o corregir reportalo, así lo podemos modificar.</p>
        </div>
        <!--Index-->
        <div id="index">
            <b><u>Indice</u></b>
            <ul>
                <li><a href="#password">Cambiar contraseña</a></li>
                <li><a href="#carga_aporte">Cargar aportes</a></li>
                <li><a href="#seguimiento">Seguimiento de aportes</a></li>
                <li><a href="#varios_meses">Cargar varios meses</a></li>
                <li><a href="#eliminar_aportes">Eliminar aportes</a></li>
                <!--<li><a href="#que_sigue">Que sigue</a></li>-->
                <li><a href="#dinero_entregado">Entregar el dinero</a></li>
                <li><a href="#estados">Estados</a></li>
                <li><a href="#filtros">Filtros</a></li>
                <li><a href="#informes">Informes</a></li>
            </ul>
        </div>
    </div>

    <!--Cómo cambiar mi contraseña-->
    <br id="password">
    <h2>¿Cómo cambiar mi contraseña?</h2>
    <p>Si estás comenzando a utilizar el sistema seguramente necesitás cambiar la contraseña por una fácil de recordar.</p>
    <p>Hay un pequeño botón. Arriba a la derecha con un candado. A través de ese botón se puede modificar la contraseña las
        veces que sea necesario.</p>
    <br>
    <img src="../assets/images/help/password.png" alt="no se ha podido cargar la imágen 'ayuda contraseña'">
    <br><br><br><br>
    <p>Nos saldrá un diálogo como este. Lo completamos y seguimos nuestro camino alegremente.</p>
    <img src="../assets/images/help/cambiar_clave.png" alt="no se ha podido cargar la imágen 'cambiar clave'">
    <!--¿Cómo cargo aportes?-->
    <hr id="carga_aporte">
    <h2>¿Cómo cargo aportes?</h2>
    <ol>
        <li>Vamos al menú de “Carga”.</li>
        <li>Completamos todos los datos del formulario.</li>
        <li>Le damos “Enviar”. Ya el aporte está cargado</li>
    </ol>
    <br>
    <img src="../assets/images/help/cargar_aporte.png" alt="no se ha podido cargar la imágen 'cargar aporte'">
    <!--¿Cómo hago un seguimiento de esos aportes que cargué?-->
    <hr id="seguimiento">
    <h2>¿Cómo hago un seguimiento de esos aportes que cargué?</h2>
    <p>Ingreso al menú "Mi Seguimiento" y puedo ver un listado con todos los aportes cargados.</p>
    <br>
    <img src="../assets/images/help/seguimiento.png" alt="no se ha podido cargar la imágen 'seguimiento'">
    <!--¿Puedo cargar varios meses de un mismo aportante a la vez?-->
    <hr id="varios_meses">
    <h2>¿Puedo cargar varios meses de un mismo aportante a la vez?</h2>
    <p>Si. Lo haremos desde la pestaña de carga. Pero en este caso seleccionaremos la opción "Rango", la cual nos va a permitir
        seleccionar más de un mes.</p>
    <p>Vemos que ahora en la parte donde ponemos el dinero no dice “Monto” sino “Monto Por Mes”. Es decir que si recibimos
        de un mismo aportante 1500 pesos por 3 meses vamos a poner “500” ya que es el monto por cada mes.</p>
    <p>Abajo también vamos a seleccionar “Desde” y “Hasta” para señalar los meses en cuestión.</p>
    <p>Lo demás es todo igual. Le damos a enviar y luego vamos a “Seguimiento” para ver si todo está bien.</p>
    <br>
    <img src="../assets/images/help/rango.png" alt="no se ha podido cargar la imágen 'rango'">
    <br><br><br>
    <p>Ya en Seguimiento podemos ver que se han cargado ese rango de 3 meses correctamente.</p>
    <br>
    <img src="../assets/images/help/seguimiento_rango.png" alt="no se ha podido cargar la imágen 'seguimiento rango'">
    <!--¿Cómo elimino un aporte que cargué incorrectamente?-->
    <hr id="eliminar_aportes">
    <h2>¿Cómo elimino un aporte que cargué incorrectamente?</h2>
    <ol>
        <li>Seleccionamos el o los aportes a eliminar.</li>
        <li>Pulsamos el botón “Eliminar”.</li>
        <li>Nos salta un cuadro de confirmación. Le damos que “Si” para confirmar.</li>
    </ol>
    <br>
    <img src="../assets/images/help/eliminar.png" alt="no se ha podido cargar la imágen 'eliminar'">
    <!--¿Qué sigue?-->
    <hr id="que_sigue">
    <h2>¿Qué sigue?</h2>
    <p>Hasta ahora solo hemos recibido aportes y los hemos cargado en el sistema. Pero hasta el momento solo es visible para
        la persona que lo cargó. Incluso vimos cómo eliminarlos si fueron cargados con datos incorrectos o quizás duplicados.
        Ahora tenemos que entregar el dinero recibido.</p>
    <!--¿Qué hago cuando ya entregué el dinero?-->
    <hr id="dinero_entregado">
    <h2>¿Qué hago cuando ya entregué el dinero?</h2>
    <p>Una vez que entregué el dinero voy a marcar esos aportes como “Entregados”. Podemos ver abajo como el sistema me informa
        que tengo $5000.- pesos recibidos y cero pesos entregados. </p>
    <p>Vamos a seleccionar todos esos aportes. Podemos aprovechar el selector que está al lado de la palabra “aportante”
        para seleccionar todos. Finalmente le damos al botón “Marcar como entregado”.</p>
    <br>
    <img src="../assets/images/help/entrega_dinero_01.png" alt="no se ha podido cargar la imágen 'entrega dinero 01'">
    <br><br><br>
    <p>Ahora vemos algunos cambios. El botón “Marcar como entregado” fue reemplazado por otro de “Marcar como no entregado”
        en el caso de que se quiera volver para atrás esa operación.</p>
    <p>Por otra parte los valores han cambiado. Ahora tenemos cero pesos recibidos, $5000.- entregados.</p>
    <p>Más a la derecha podemos ver que la columna “Entrega” ahora tiene la fecha actual. Y finalmente vemos que la etiqueta
        “Recibido” a cambiado a “Entregado” para todas esas filas.</p>
    <img src="../assets/images/help/entrega_dinero_02.png" alt="no se ha podido cargar la imágen 'entrega dinero 02'">
    <!--Estados-->
    <hr id="estados">
    <h2>Estados</h2>
    <p>Hay 3 tipos de estado. Y estos son Recibido, Entregado y Confirmado.</p>
    <p><b>Recibido:</b> Cuando recibí un aporte y lo ingresé.</p>
    <p><b>Entregado:</b> Cuando se lo entregué a Carina o a alguien de la comisión.</p>
    <p><b>Confirmado:</b> Cuando Carina confirma la recepción y lo marca como “Confirmado”, cerrando así el ciclo completo.</p>
    <br>
    <img src="../assets/images/help/estados.png" alt="no se ha podido cargar la imágen 'estados'">
    <!--Filtros-->
    <hr id="filtros">
    <h2>Filtros</h2>
    <p>En las pestañas de "Mi seguimiento" y en "Histórico" tenemos la posibilidad de filtrar información de la base de datos.</p>
    <p><b>Mi Seguimiento:</b> Es el listado de aportes que recibí desde que soy receptor.</p>
    <p><b>Histórico:</b> Es el listado de todos los aportes recibidos por todos los recaudadores en todo período.</p>
    <p><em>Estas opciones son muy útiles por ejemplo para buscar a un maestro en particular que nos consulta cuándo fue su
            último aporte, etc.</em></p>
    <br>
    <p>A continuación vemos el botón de filtro.</p>
    <img src="../assets/images/help/filtros_01.png" alt="no se ha podido cargar la imágen 'filtros'">
    <br><br>
    <p>Y las opciones de filtro ya desplegadas.</p>
    <img src="../assets/images/help/filtros_02.png" alt="no se ha podido cargar la imágen 'filtros'">
    <!--Informes-->
    <hr id="informes">
    <h2>Informes</h2>
    <p>Para finalizar en la pestaña informes podemos crear una síntesis de los aportes recibidos por todos los receptores
        en el último semestre o en el período deseado.</p>
    <p>El campo "umbral" hace referencia al valor de referencia fijado en las reuniones. De este modo podemos tener el dato
        por ejemplo de cuántos aportes están por debajo de ese valor.</p>
    <br>
    <img src="../assets/images/help/informes.png" alt="no se ha podido cargar la imágen 'informes'">
    <!--Presentación-->
    <hr>
    <h2>Por el momento es todo</h2>
    <p>Si encontrás algún tema que no fue tenido en cuenta en el tutorial escribinos a:</p>
    <ul>
        <li>Santiago González Rojo: <a href="mailto:gonzalezrojosantiago@gmail.com" target="_top">gonzalezrojosantiago@gmail.com</a></li>
        <br>
        <li>Carlos Muñoz: <a href="mailto:carlosdamian.munoz@gmail.com" target="_top">carlosdamian.munoz@gmail.com</a></li>
        <br>
    </ul>
</main>