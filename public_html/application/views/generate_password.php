<main>
<?php if (isset($new_password)) { ?>
<div class="new_password_legend">
	Su contraseña temporal es<br><?php echo $new_password; ?><br><br>
	ATENCIÓN! Esta contraseña expirará en 7 días.<br>Cambie su contraseña ingresando al link que se encuentra al lado de su nombre.<br>
</div>
<a class="blue-link" href="/login" target="_blank" title="Iniciar sesión">Iniciar sesión</a>
<?php } else if (isset($error)) { ?>
<div>
	<?php echo $error['message']; ?>
</div>
<?php } ?>
</main>
