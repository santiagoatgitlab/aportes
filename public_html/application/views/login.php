<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
	<form id="login-box" method="post" action="">
		<h2>Iniciar Sesión</h2>
		<div>
			<input type="text" placeholder="email" name="email" value="<?php echo $email; ?>" required>
		</div>
		<div>
			<input type="password" placeholder="contraseña" name="password" required>
		</div>
		<div>
			<button title="Ingresar">Ingresar</button>
		</div>
	</form>
	<?php if ($error) { ?>
	<div class="error-message">Email o contraseña incorrectos</div>
	<?php } ?>
	<a class="blue-link" href="/recuperar_clave" title="Generar una contraseña temporal para poder ingresar al sistema">Olvidé mi contraseña</a>
</main>

