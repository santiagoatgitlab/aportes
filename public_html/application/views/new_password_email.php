<div>
	Bienvenido/a al sistema de aportes!<br>
	Haz click en el siguiente link para ingresar al sistema<br>
	<a href="https://aportes.maestrosplr.net">https://aportes.maestrosplr.net</a><br><br>
	Tus datos de acceso son los siguientes:<br>
	<strong>Email:</strong> <?php echo $email; ?><br>
	<strong>Contraseña:</strong> <?php echo $password; ?><br><br>
	Recuerda que puedes cambiar tu contraseña haciendo click en el candado que se encuentra a la izq de tu nombre una vez que estés logueado
</div>

