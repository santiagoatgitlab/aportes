<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main id="reports">
	<form id="reports-box" method="get" action="">
		<h2>Informes</h2>
		<div>
			<div><label>Umbral</label></div>
			<div><input type="number" name="threshold" required></div>
		</div>
		<div>
			<div><label>Período</label></div>
			<div><label>Desde</label></div>
			<div>
				<select name="month_from">
					<option value="">Mes</option>
					<?php foreach ($month_names as $index => $name) { ?>
						<option value="<?php echo $index+1; ?>"><?php echo $name; ?></option>
					<?php } ?>
				</select>	
				<select name="year_from">
					<option value="">Año</option>
					<?php for($year=FIRST_YEAR; $year <= $current_year; $year++) { ?>
						<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
					<?php } ?>
				</select>	
			</div>
			<div><label>Hasta</label></div>
			<div>
				<select name="month_to">
					<option value="">Mes</option>
					<?php foreach ($month_names as $index => $name) { ?>
						<option value="<?php echo $index+1; ?>"><?php echo $name; ?></option>
					<?php } ?>
				</select>	
				<select name="year_to">
					<option value="">Año</option>
					<?php for($year=FIRST_YEAR; $year <= $current_year; $year++) { ?>
						<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
					<?php } ?>
				</select>	
				</select>	
			</div>
		</div>
		<div>
			<button title="Generar informe">Generar informe</button>
		</div>
	</form>
</main>
