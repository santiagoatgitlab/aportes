<style>
	main{
		display: flex;
		font-family: verdana;
		flex-direction: column;
		align-items: center;
	}
	main > div{
		width: 900px;
	}
	table{
		box-shadow: 2px 2px 5px rgba(0,0,0,0.5);
		font-weight: normal;
		margin-top: 80px;
		margin-bottom: 80px;
	}
	td{
		padding: 5px;
		text-align: center;
	}
	.header2 td{
		width: 120px;
	}
	tr:nth-child(odd){
		background-color: #ccc;
	}
	td.money{
		text-align: right;
	}
	td.left{
		text-align: left;
	}
	td.total{
		font-weight: bold;
	}
	tr.total td, tr.average td{
		background-color: #555;
		padding: 10px 16px;
		color: #fff;
	}
	.header1 td{
		background-color: rgba(250,140,0,1);
		color: #fff;
		font-weight: bold;
	}
	.header2 td{
		background-color: rgba(250,140,0,1);
		color: #fff;
		font-weight: bold;
	}
	.graphic{
		box-shadow: 2px 2px 5px rgba(0,0,0,0.5);
		display: flex;
		padding: 50px;
		height: 400px;
		align-items: flex-end;
		margin-bottom: 80px;
	}
	.graphic .amounts{
		height: 100%;
		display: flex;
		flex-direction: column;
		justify-content: space-between;
	}
	.graphic .bar{
		background-color: #FA8C00;
		width: <?php echo 420/count($report['months']); ?>px;
		height: <?php echo (400/100*40); ?>px;
		margin-left: <?php echo 240/count($report['months']); ?>px;
		box-shadow: 2px 2px 10px #888;
	}
	<?php for ($i=1; $i <= count($report['months']); $i++) { ?>
	.graphic .bar:nth-child(<?php echo $i+1 ?>){ height: <?php echo round( 3*(100/$report['highest_month_amount']*$report['months'][$i-1]['total_sum']) ); ?>px; }
	<?php } ?>
</style>
<body>
	<main>
		<div>
			<table cellpadding="0" cellspacing="0">
			<tr class="header1">
				<td rowspan="2">Mes</td>
				<td colspan="2">Aportantes de -<?php echo $threshold; ?></td>
				<td colspan="2">Aportantes de +<?php echo $threshold; ?></td>
				<td colspan="2">Total</td>
			</tr>
			<tr class="header2">
				<td>Cantidad</td>
				<td>Monto</td>
				<td>Cantidad</td>
				<td>Monto</td>
				<td>Cantidad</td>
				<td>Monto</td>
			</tr>
			<?php foreach ($report['months'] as $row) { ?>
				<tr>
					<td class="left"><?php echo $month_names[$row['month']-1] . ' ' . $row['year']; ?></td>
					<td><?php echo isset($row['low_count']) ? number_format($row['low_count'],0,",",".") : 0; ?></td>
					<td class="money">$<?php echo isset($row['low_sum']) ? number_format($row['low_sum'],0,",",".") : 0; ?></td>
					<td><?php echo isset($row['high_count']) ? number_format($row['high_count'],0,",",".") : 0; ?></td>
					<td class="money">$<?php echo isset($row['high_sum']) ? number_format($row['high_sum'],0,",",".") : 0; ?></td>
					<td><?php echo isset($row['total_count']) ? number_format($row['total_count'],0,",",".") : 0; ?></td>
					<td class="money">$<?php echo isset($row['total_sum']) ? number_format($row['total_sum'],0,",",".") : 0; ?></td>
				</tr>
			<?php } ?>
			<tr class="total">
				<td colspan="6" class="left total">Total</td>
				<td class="money total">$<?php echo number_format($report['total_amount'],0,",","."); ?></td>
			</tr>
			<tr class="average">
				<td colspan="6" class="left">Promedio Mensual</td>
				<td class="money">$<?php echo number_format($report['total_amount'] / count($report['months']),0,",","."); ?></td>
			</tr>
			</table>
		</div>
		<div class="graphic-container">
			<h3><?php echo $month_names[$report['months'][0]['month']-1] . " " . $report['months'][0]['year'] . " a "
			 . $month_names[$report['months'][count($report['months'])-1]['month']-1] . " "
			 . $report['months'][count($report['months'])-1]['year']; ?></h3>
			<div class="graphic">
				<div class="amounts">
				<?php for ($i=0; $i <= 6; $i++) { ?>
					<div>$ <?php echo number_format($report['highest_month_amount'] / 6 * (6-$i),0,",","."); ?></div>
				<?php } ?>
				</div>
				<?php foreach ($report['months'] as $row) { ?>
				<div class="bar">&nbsp;</div>
				<?php } ?>
			</div>
		</div>
	</main>
</body>
