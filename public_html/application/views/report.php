<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main id="reports">
	<div>
		<table cellpadding="0" cellspacing="0">
		<tr class="header1">
			<td rowspan="2">Mes</td>
			<td colspan="2">Aportantes de -<?php echo $threshold; ?></td>
			<td colspan="2">Aportantes de +<?php echo $threshold; ?></td>
			<td colspan="2">Total</td>
		</tr>
		<tr class="header2">
			<td>Cantidad</td>
			<td>Monto</td>
			<td>Cantidad</td>
			<td>Monto</td>
			<td>Cantidad</td>
			<td>Monto</td>
		</tr>
		<?php foreach ($report['months'] as $row) { ?>
			<tr>
				<td class="left"><?php echo $month_names[$row['month']-1] . ' ' . $row['year']; ?></td>
				<td><?php echo isset($row['low_count']) ? number_format($row['low_count'],0,",",".") : 0; ?></td>
				<td class="money">$<?php echo isset($row['low_sum']) ? number_format($row['low_sum'],0,",",".") : 0; ?></td>
				<td><?php echo isset($row['high_count']) ? number_format($row['high_count'],0,",",".") : 0; ?></td>
				<td class="money">$<?php echo isset($row['high_sum']) ? number_format($row['high_sum'],0,",",".") : 0; ?></td>
				<td><?php echo isset($row['total_count']) ? number_format($row['total_count'],0,",",".") : 0; ?></td>
				<td class="money">$<?php echo isset($row['total_sum']) ? number_format($row['total_sum'],0,",",".") : 0; ?></td>
			</tr>
		<?php } ?>
		<tr class="total">
			<td colspan="6" class="left total">Total</td>
			<td class="money total">$<?php echo number_format($report['total_amount'],0,",","."); ?></td>
		</tr>
		<tr class="average">
			<td colspan="6" class="left">Promedio Mensual</td>
			<td class="money">$<?php echo number_format($report['total_amount'] / count($report['months']),0,",","."); ?></td>
		</tr>
		</table>
	</div>
	<div class="graphic-container">
		<div id="bar_heights">
			<?php for ($i=1; $i <= count($report['months']); $i++) { ?>
			<input type="hidden" name="bar_<?php echo $i ?>" value="<?php echo round( 3*(100/$report['highest_month_amount']*$report['months'][$i-1]['total_sum']) ); ?>">
			<?php } ?>
		</div>
		<h3><?php echo $month_names[$report['months'][0]['month']-1] . " " . $report['months'][0]['year'] . " a "
		 . $month_names[$report['months'][count($report['months'])-1]['month']-1] . " "
		 . $report['months'][count($report['months'])-1]['year']; ?></h3>
		<div class="graphic">
			<div class="amounts">
			<?php for ($i=0; $i <= 6; $i++) { ?>
				<div>$ <?php echo number_format($report['highest_month_amount'] / 6 * (6-$i),0,",","."); ?></div>
			<?php } ?>
			</div>
			<?php for ($i=1; $i <= count($report['months']); $i++) { ?>
			<div class="bar" data-height="<?php echo round( 3*(100/$report['highest_month_amount']*$report['months'][$i-1]['total_sum']) ); ?>">&nbsp;</div>
			<?php } ?>
		</div>
		<div class="graphic-values">
			<input type="hidden" class="bar-width" value="<?php echo 420/count($report['months']).'px'; ?>">
			<input type="hidden" class="bar-margin-left" value="<?php echo 240/count($report['months']).'px'; ?>">
		</div>
	</div>
</main>
