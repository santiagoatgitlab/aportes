<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<?php if(isset($notifications)) foreach($notifications as $notification){ ?>
	<div class="floating-notification real <?php echo $notification['type']; ?>"><div class="content"><?php echo $notification['content']; ?></div></div>
	<?php } ?>
<main id="load">

	<form id="load-box" method="post" action="">
		<h2>Cargar aporte</h2>
		<div>
			<div><label>Aportante</label></div>
			<div class="select-input">
				<input type="text" id="contributor_name" list="contributors" id="contributor_name" value="" autocomplete="off">
				<input type="hidden" id="contributor_id" name="contributor_id" value="" required>
				<datalist id="contributors">
					<?php foreach($contributors as $contributor) { ?>
					<option data-id="<?php echo $contributor['id']; ?>" value="<?php echo $contributor['contributor']; ?>"></option>
					<?php } ?>	
				</datalist>
			</div>
		</div>
		<div>
			<div><label id="monto_label">Monto</label></div>
			<div><input type="number" name="amount" required></div>
		</div>
		<div>
			<div><label>Período</label></div>
			<input type="radio" name="frame_type" class="month" value="month" checked>Mes<input type="radio" name="frame_type" class="range" value="range">Rango
			<div class="frame-month">
				<div>
					<select name="month[month]">
						<option value="">Mes</option>
						<?php for ($i=0; $i < count($month_names); $i++) { ?>
						<option value="<?php echo ($i+1); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[1]) == ($i+1)) ? "selected" : "" ?>><?php echo $month_names[$i]; ?></option>
						<?php } ?>
					</select>	
					<select name="month[year]">
						<option value="">Año</option>
						<?php for ($i=intval(date('Y'))+2; $i>=FIRST_YEAR; $i--) { ?>
						<option value="<?php echo ($i); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[0]) == ($i)) ? "selected" : "" ?>><?php echo $i; ?></option>
						<?php } ?>
					</select>	
				</div>
			</div>
			<div class="frame-range">
				<div><label>Desde</label></div>
				<div>
					<select name="range[month_from]">
						<option value="">Mes</option>
						<?php for ($i=0; $i < count($month_names); $i++) { ?>
						<option value="<?php echo ($i+1); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[1]) == ($i+1)) ? "selected" : "" ?>><?php echo $month_names[$i]; ?></option>
						<?php } ?>
					</select>	
					<select name="range[year_from]">
						<option value="">Año</option>
						<?php for ($i=intval(date('Y'))+2; $i>=FIRST_YEAR; $i--) { ?>
						<option value="<?php echo ($i); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[0]) == ($i)) ? "selected" : "" ?>><?php echo $i; ?></option>
						<?php } ?>
					</select>	
				</div>
				<div><label>Hasta</label></div>
				<div>
					<select name="range[month_to]">
						<option value="">Mes</option>
						<?php for ($i=0; $i < count($month_names); $i++) { ?>
						<option value="<?php echo ($i+1); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[1]) == ($i+1)) ? "selected" : "" ?>><?php echo $month_names[$i]; ?></option>
						<?php } ?>
					</select>	
					<select name="range[year_to]">
						<option value="">Año</option>
						<?php for ($i=intval(date('Y'))+2; $i>=FIRST_YEAR; $i--) { ?>
						<option value="<?php echo ($i); ?>" <?php echo (isset($filters['periodo-min']) && intval(explode('-',$filters['periodo-min']['value'])[0]) == ($i)) ? "selected" : "" ?>><?php echo $i; ?></option>
						<?php } ?>
					</select>	
				</div>
			</div>
		</div>
		<div class="receptionDate">
			<div><label>Recibido el</label></div>
			<input class="date" type="text" name="reception_date" id="reception_date" required>
		</div>
		<div>
			<button title="Cargar aporte">Enviar</button>
		</div>
	</form>
</main>
