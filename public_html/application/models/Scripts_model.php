<?php 
class Scripts_model extends CI_Model {

	var $repetidos = array();
	var $meses = array('ene'=>'01','feb'=>'02','mar'=>'03','abr'=>'04','may'=>'05','jun'=>'06','jul'=>'07','ago'=>'08','sep'=>'09','oct'=>'10','nov'=>'11','dic'=>'12');
	var $max_contributions_id = 11314;

	public function do_queries($lines){
		
		$outputContent = array();
		for($i=1;$i<count($lines);$i++){
			$query 	= "insert into contributions (user_id,contributor_id,period,amount,reception_date,transference_date,status) values ";
			$values = $this->getLineValues($lines[$i]);
			if ($values !== false){
				$query .= $values . ";" . PHP_EOL;
				$outputContent[] = $query;
			}
		}
		return array(
			"queries" => $outputContent,
			"repetidos" => $this->repetidos
		);	


	}

	function getLineValues($line){
		$fields 		= explode(',',$line);
		$user_id 		= $this->getUserId($fields[1]);
		$contributor_id = $this->getContributorId($fields[2]);
		if ($user_id > 0 && $contributor_id > 0){
			$result = "(";
			$result .= $user_id . ',';
			$result .= $contributor_id . ',';
			$result .= '"' . $this->parsePeriod($fields[3]) . '",';
			/*$result .= '"' . $this->parseStringPeriod($fields[3]) . '",';*/
			$result .= $this->parseAmount($fields[4]) . ',';
			$result .= '"' . $this->parseReceptionDate($fields[0]) . '",';
			$result .= '"2018-08-12",';
			$result .= "3";
			$result .= ")";
			return $result;
		}
		return false;
	}

	function getUserId($name){
		echo $name ; echo "<br>" ; 
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('concat(name," ",last_name) = ' . '"' . $name . '"');
		$result = $this->db->get()->result_array();
		if (count($result) > 0 ){
			return intval($result[0]['id']);
		}
		else{
			$repetido = "usuario " . $name . " no se encuentra en la base de datos | como recaudador\n";
			if (!in_array($repetido,$this->repetidos)){
				$this->repetidos[] = $repetido;
			}
			return 0;
		}
	}

	function getContributorId($name){
		$query 	= "select id from contributors where name = " . '"' . $name . '";';
		$this->db->select('id');
		$this->db->from('contributors');
		$this->db->where('name = ' . '"' . $name . '"');
		$result = $this->db->get()->result_array();
		if (count($result) > 0 ){
			return intval($result[0]['id']);
		}
		else{
			$repetido = "usuario " . $name . " no se encuentra en la base de datos | como aportante\n";
			if (!in_array($repetido,$this->repetidos)){
				$this->repetidos[] = $repetido;
			}
			return 0;
		}
	}

	function parsePeriod($value){
		echo "periodo: " . $value . "<br>";
		$tokens = explode('/' , $value);
		$day  	= "01";
		$month 	= $tokens[0];
		$year  	= "20" . $tokens[1];
		return $year . "-" . $month . "-" . $day;
	}

	function parseStringPeriod($value){
		$tokens = explode('-' , $value);
		$day  	= "01";
		$month 	= $this->meses[$tokens[0]];
		$year  	= "20" . $tokens[1];
		return $year . "-" . $month . "-" . $day;
	}

	function parseAmount($value){
		return intval(trim(substr($value,3)));
	}

	function parseReceptionDate($value){
		$tokens = explode('/' , $value);
		$day  	= $tokens[0];
		$month 	= $tokens[1];
		$year  	= "20" . $tokens[2];
		return $year . "-" . $month . "-" . $day;
	}
		
	function generate_duplicateds_list(){
		$ya_encontrados = array();
		$duplicaciones_completo = array();
		for($id=1;$id<=$this->max_contributions_id;$id++){
			if (!in_array($id,$ya_encontrados)){
				$contribution_result = $this->db->select('user_id,contributor_id,period,amount,reception_date')->from('contributions')->where('id', $id)->get();
				if ($contribution_result && ($contribution_result->result_array() > 0)){
					if (isset($contribution_result->result_array()[0])){
						$contribution = $contribution_result->result_array()[0];
						$duplications_result = $this->db->select('id')->from('contributions')->where($contribution)->get();
						if ($duplications_result){
							$duplications = $duplications_result->result_array();
							$ids = $id . ',';
							foreach($duplications as $duplication){
								if ($duplication['id'] != $id){
									$ids .= $duplication['id'] . ',';
									if (isset($duplicaciones_completo[$id])){
										$duplicaciones_completo[$id][] = $duplication['id'];
									}
									else{
										$duplicaciones_completo[$id] = array($duplication['id']);
									}
									$ya_encontrados[] = $duplication['id'];
								}
							}
							if (count($duplications) > 1){
							}
						}
					}
					else{
						echo "caso raro: " . $this->db->last_query() . '<br>';
					}
				}
			}
		}
		echo count($duplicaciones_completo) . " aportes se encuentran duplicados";
		return $duplicaciones_completo;
	}
} 
