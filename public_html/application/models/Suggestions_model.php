<?php 
class Suggestions_model extends CI_Model {

	public function get_collectors(){
		$this->db->select('distinct(concat(name," ",last_name)) as collector');
		$this->db->from('users');
		return $this->db->get()->result_array();	
	}

	public function get_contributors($user_id = null){
		$this->db->select('id,name as contributor');
		$this->db->from('contributors');
		return $this->db->get()->result_array();	
	}
}
