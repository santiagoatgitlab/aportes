<?php 
class Reports_model extends CI_Model {

	public function get_report($threshold,$period_from,$period_to){

		$this->db->start_cache();
		$this->db->select('count(*) as count, transference_date, sum(amount) as sum, month(transference_date) as month, year(transference_date) as year');
		$this->db->from('contributions');
		$this->db->where('concat(year(transference_date),"-",if(month(transference_date)<10,"0",""),month(transference_date),"-01") >=',$period_from);
		$this->db->where('concat(year(transference_date),"-",if(month(transference_date)<10,"0",""),month(transference_date),"-01") <',$period_to);
		$this->db->group_by('concat(year(transference_date),"-",if(month(transference_date)<10,"0",""),month(transference_date))');
		$this->db->order_by('concat(year(transference_date),"-",if(month(transference_date)<10,"0",""),month(transference_date))');
		$this->db->stop_cache();

		$total_result =  $this->db->get()->result_array();

		$this->db->where('amount <', $threshold);
		$low_result =  $this->db->get()->result_array();

		$this->db->where('amount >=', $threshold);
		$high_result =  $this->db->get()->result_array();

		$result = array();
		$low_index = $high_index = 0;
		$total_amount = 0;
		for($i=0; $i < count($total_result); $i++){
			if (!empty($low_result)){
				if (isset($low_result[$low_index]) && $total_result[$i]['month'] == $low_result[$low_index]['month']){
					$result[$i]['low_count'] = $low_result[$low_index]['count'];
					$result[$i]['low_sum'] = $low_result[$low_index]['sum'];
					$low_index++;
				}
				else{
					$result[$i]['low_count'] = 0;
					$result[$i]['low_sum'] = 0;
				}
			} 
			if (!empty($high_result)){
				if (isset($high_result[$high_index]) && $total_result[$i]['month'] == $high_result[$high_index]['month']){
					$result[$i]['high_count'] = $high_result[$high_index]['count'];
					$result[$i]['high_sum'] = $high_result[$high_index]['sum'];
					$high_index++;
				}
				else{
					$result[$i]['high_count'] = 0;
					$result[$i]['high_sum'] = 0;
				}
			} 
			$result[$i]['total_count'] = $total_result[$i]['count'];
			$result[$i]['total_sum'] = $total_result[$i]['sum'];
			$result[$i]['month'] = $total_result[$i]['month'];
			$result[$i]['year'] = $total_result[$i]['year'];
			$total_amount += $total_result[$i]['sum'];
		}

		return ['months'=>$result, 'total_amount'=>round($total_amount,3)];
		
	}
}


