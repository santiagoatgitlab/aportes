<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scripts extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function doQueries()
	{
		$this->load->model('Scripts_model','',TRUE);

		$files = scandir(APPPATH . '../public/files/source');
		
		for($i=2; $i<count($files); $i++){
			$file = $files[$i];
			$input_file	= APPPATH . '../public/files/source/' . $file;
			$lines			= file($input_file);
			$output 		= $this->Scripts_model->do_queries($lines);
			$queries_file	= str_replace(".csv",".sql",$file);
			echo "<br><b>" . $file . "</b><br>";
			echo count($output['queries']) . ' contribuciones a insertar<br>';
			echo count($output['repetidos']) . ' nombres no encontrados<br>';
			file_put_contents(APPPATH . '../public/files/queries/' . $queries_file, $output['queries']);
			file_put_contents(APPPATH . '../public/files/repetidos.txt', $output['repetidos'], FILE_APPEND);
		}
	}
	
	public function generateDuplicatedsList(){
		$this->load->model('Scripts_model','',TRUE);
		$list = $this->Scripts_model->generate_duplicateds_list();
		// echo '<pre>'; var_dump($list); echo '<pre>';
		$ids = "";
		$idsRemove = "";
		foreach($list as $key => $dup){
			$ids .= $key . ',';	
			foreach($dup as $id_dup){
				$ids .= $id_dup . ',';
				$idsRemove .= $id_dup . ',';
			}
		}
		echo '<br><br>';
		echo 'select cs.id,period,amount,reception_date,concat(u.name," ",u.last_name) as recaudador,cr.name  as aportante from contributions cs left join users u on user_id = u.id left join contributors cr on cr.id = contributor_id where cs.id in(' . trim($ids,',') . ') order by amount,aportante, recaudador, reception_date, period; <br>';
		echo '<br><hr><br>';
		echo 'aportes a eliminar: ' . count(explode(',',trim($idsRemove,',')));
		echo '<br><br>';
		echo 'delete from contributions where id in(' . trim($idsRemove,',') . ') ';
	}
	
}
