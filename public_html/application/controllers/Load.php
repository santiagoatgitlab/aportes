<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Load extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $month_names 	= [ 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre' ];

	public function index()
	{
		$this->load->model('Load_model','',TRUE);
		$this->load->model('Suggestions_model','',TRUE);
		$this->check_log();

		$this->data['month_names'] 	= $this->month_names; 
		$contributors = $this->Suggestions_model->get_contributors();
		for($i=0;$i<count($contributors);$i++){
			$contributors[$i]['contributor'] = ucwords($this->replaceAccents(strtolower($contributors[$i]['contributor'])));
		}
		$this->data['contributors'] = $contributors;

		if ( !empty($this->input->post()) ){
		
			$post = $this->input->post();
			$reception_date_array = explode('/',$post['reception_date']);
			$post['reception_date'] = $reception_date_array[2] . '-' . $reception_date_array[1] . '-' . $reception_date_array[0]; 
			if ($post['frame_type'] == 'month'){
				$current_month = $post['month']['month'];
				$current_year  = $post['month']['year'];
			}
			else{
				$current_month = $post['range']['month_from'];
				$current_year  = $post['range']['year_from'];
			}

			$period = $current_year . "-";
			if ($current_month < 10){ $period .= "0"; }
			$period .= $current_month . "-01";

			$insert_count = 0;
			if ($post['frame_type'] == 'month'){
				$insert_success = $this->Load_model->save($this->data['userdata']['id'],$post['reception_date'],$post['contributor_id'],$period,$post['amount']);
				if ($insert_success){
					$insert_count++;
				}
			}
			else{
				$insert_success = $this->Load_model->save($this->data['userdata']['id'],$post['reception_date'],$post['contributor_id'],$period,$post['amount']); 
				if ($insert_success){
					$insert_count++;
				}
				while ($current_month < $post['range']['month_to'] || $current_year < $post['range']['year_to']){
					$current_month++;
					if ($current_month > 12){
						$current_year++;
						$current_month = 1;
					}

					$period = $current_year . "-";
					if ($current_month < 10){ $period .= "0"; }
					$period .= $current_month . "-01";

					if (!$this->Load_model->save($this->data['userdata']['id'],$post['reception_date'],$post['contributor_id'],$period,$post['amount'])){
						$insert_success = false;
					}
					else{
						$insert_count++;
					}
				}
			}
			if ($insert_success){
				$_SESSION['saved_rows'] = $insert_count;
				$this->session->mark_as_flash('saved_rows');
			}
			redirect('/carga');
		}
		if (isset($_SESSION['saved_rows'])){
			if ($_SESSION['saved_rows'] == 1){
				$this->data['notifications'] = [
					["type"=>"success", "content"=>"Se ha guardado ". $_SESSION['saved_rows'] . " aporte exitosamente"]
				];
			}
			else{
				$this->data['notifications'] = [
					["type"=>"success", "content"=>"Se han guardado ". $_SESSION['saved_rows'] . " aportes exitosamente"]
				];
			}
		}

		$this->data['stylesheets'] 	= ['load.css'];
		$this->data['scripts'] 		= ['load.js'];

		$this->layout('load');
	}

	private function replaceAccents($string){
		$string = str_replace('á','a',$string);
		$string = str_replace('é','e',$string);
		$string = str_replace('í','i',$string);
		$string = str_replace('ó','o',$string);
		$string = str_replace('ú','u',$string);
		return $string;
	}
}
