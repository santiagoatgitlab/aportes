<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		/*echo password_hash('albatros', PASSWORD_BCRYPT);*/
		$this->load->model('Log_model','',TRUE);
		$this->data['error'] = false;
		$this->data['email'] = '';
		$this->data['stylesheets'] = ['login.css'];
		$this->data['scripts'] = [];
		
		if (isset($this->session->id)){
			if ($this->data['userdata']['role'] == 1){
				header('location: /load');
			}
			else{
				header('location: /seguimiento/o_id_desc');
			}
			
		}
		
		
		if ( !empty($this->input->post()) ){
			
			$user_data = $this->Log_model->check_log_user($this->input->post('email'),$this->input->post('password'));
			
			if (!empty($user_data)){
				
				$this->session->set_userdata($user_data);
				if ($this->data['userdata']['role'] == 1){
					header('location: /load');
				}
				else{
					header('location: /seguimiento/o_id_desc');
				}
				
			}
			else{
				$this->data['error'] = true;
				$this->data['email'] = $this->input->post('email');
			}
		}
		
		$this->layout('login');
	}
	
	public function logout(){
		
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('last_name');
		$this->session->unset_userdata('role');
		header('location: /login');
		
	}

}
