<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
 { 
	
	var $data = array();
	
	public function check_log(){

		if (!isset($this->session->id)){
			header('location: /');
		}
		else{
			$this->data['userdata'] = $this->session->userdata();
		}
		
	}	 
	 
	   //Load layout    
	public function layout($view,$data=array()) {
		 // making template and send data to view.
		$data = array_merge($this->data,$data);
		$data['main'] = $this->load->view($view,$data, true);
		$this->load->view('layout',$data);
   	}

}
