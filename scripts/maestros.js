const fs 	= require("file-system");
const mysql	= require("mysql");

const filePath = "/var/www/aportes.local/scripts/files/maestros.csv";

const mysqlhost	= "localhost";
const mysqluser	= "santiago";
const mysqlpass	= "";
const mysqldb	= "aportes";

// -- vars

let mysql_conn 			= "";
let aportantes_names 	= [];

// -- functions

function createMysqlConnection(){
	mysql_conn = mysql.createConnection({
		host		: mysqlhost,
		user		: mysqluser,
		password	: mysqlpass,
		database	: mysqldb
	});
	mysql_conn.connect();
	onMysqlConnectionCreated();
}

function getAportantesNames(){
	let query = "select name from contributors";
	mysql_conn.query(query,function(error,result){
		if (error) throw error;
		for (i=0;i<result.length;i++){
			aportantes_names.push(result[i].name);
		}
		onAportantesNamesObtained(result);
	});
}

function readFile(){
	fs.readFile(filePath, "utf8", (error,fileContent) => {
		onFileRead(fileContent.split("\n"));
	});
}

function createQueries(lines){
	let emailcounter 	= 1;
	let firstline		= true;	
	for(i=0;i<lines.length;i++){
		let line 		= lines[i];
		let fields		= line.split(",");
		let last_name 	= fields[1];
		let first_name	= fields[2];
		let name		= first_name + " " + last_name;
		let email		= fields[3];
		let existant	= false;
		if (name.trim() != "" && aportantes_names.indexOf(name) == -1 ){
			if (typeof email != "undefined" && email.trim() == ""){
				email = "email" + emailcounter + "@mail.com";
				emailcounter++;
			}
			if (!firstline){
				console.log("INSERT INTO contributors (name, first_name, last_name, email) VALUES ('"+name+"','"+first_name+"','"+last_name+"','"+email+"');");
			}
			firstline = false;
		}
	}
	onQueriesCreated();
}

// -- callbacks

function onMysqlConnectionCreated(){
	getAportantesNames();
}

function onAportantesNamesObtained(data){
	readFile();
}

function onFileRead(lines){
	createQueries(lines);
}

function onQueriesCreated(){
	process.exit();
}

// -- program initialization

createMysqlConnection();
