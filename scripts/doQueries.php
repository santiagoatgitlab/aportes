<?php 

$servername = "localhost";
$username 	= "root";
$password	= "unitiva";
$dbname		= "aportes_la_reja";

$input_file	= $argv[1];
$output		= str_replace($input_file,".php",".sql");

$conn = new mysqli($servername,$username,$password,$dbname);

if ($conn->connect_error){
	die("connection failed: " . $conn->connect_error);
}

consoleLog("connected successfully");
consoleLog("reading input: " . $input_file);

#
# main  -----------------------------------------------------------------------
#

$lines = file($input_file);
$outputContent = array();
for($i=1;$i<count($lines);$i++){
	$query = "insert into contributions (id,user_id,contributor_id,period,amount,reception_date,transference_date') values ";
	$query = getLineValues($lines[$i],$conn) . ";";
	$outputContent[] = $query;
}
file_put_contents($output);

consoleLog("process finished");

#
# funciones  -----------------------------------------------------------------------
#

function getLineValues($line,$conn){
	$fields = explode(',',$line);
	getUserId($fields[1],$conn);
	/*
	$result = "(";
	$result = '"' . getUserId($fields[1],$conn) . '",';
	$result = '"' . getContributorId($fields[2],$conn) . '",';
	$result = '"' . parsePeriod($fields[3]) . '",';
	$result = '"' . parseAmount($fields[4]) . '",';
	$result = '"' . parseReceptionDate($fields[0]) . '",';
	$result = '"2018/07/08",';
	$result = "3,";
	$result = ")";
	return $result;
	*/
}

function getUserId($name,$conn){
	$query 	= 'select id from users where concat(name," ",last_name) = ' . '"' . $name . '";';
	$query 	= 'select id from users;';
	consoleLog($query);
	$result	= $conn->query($query);
	consoleLog("num rows: " . $result->num_rows);
	echo "result 1: " . var_dump($result->fetch_assoc());
	die();
	if ($result->num_rows > 0){
		$id 	= $result->fetch_assoc()[0]["id"];
		return $id;
	}
	throw new Exception("error en la query: " . $query);
}

function getContributorId($name,$conn){
	$query 	= "select id from contributors where name = " . '"' . $name . '";';
	$result	= $conn->query($query);
	echo var_dump($result);
	if ($result && $result->num_rows > 0){
		$id 	= $result->fetch_assoc()[0]["id"];
		return $id;
	}
	throw new Exception("error en la query: " . $query);
}

function parsePeriod($value){
	$tokens = explode('/' . $value);
	$day  	= "01";
	$month 	= $tokens[0];
	$year  	= "20" . $tokens[1];
	return $year . "-" . $month . "-" . $day;
}

function parseAmount($value){
	return inval(trim(substr($value,0,3)));
}

function parseReceptionDate($value){
	$tokens = explode('/' . $value);
	$day  	= $tokens[0];
	$month 	= $tokens[1];
	$year  	= "20" . $tokens[2];
	return $year . "-" . $month . "-" . $day;
}

#
# funciones extra ------------------------------------------------------------------
#

function consoleLog($message){
	echo $message . PHP_EOL;
}

?>
