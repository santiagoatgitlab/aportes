-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: aportes_la_reja
-- ------------------------------------------------------
-- Server version	10.1.29-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `aportes_la_reja`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `aportes_la_reja` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `aportes_la_reja`;

--
-- Table structure for table `contributions`
--

DROP TABLE IF EXISTS `contributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contributor` varchar(256) NOT NULL,
  `period` date NOT NULL,
  `amount` int(11) NOT NULL,
  `reception_date` date NOT NULL,
  `transference_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `collector` (`user_id`),
  CONSTRAINT `collector` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contributions`
--

LOCK TABLES `contributions` WRITE;
/*!40000 ALTER TABLE `contributions` DISABLE KEYS */;
INSERT INTO `contributions` VALUES (6,1,'Diego Dorado','2014-11-01',600,'2018-02-14','0000-00-00 00:00:00',1),(7,1,'Domingo Dominguez','2013-03-01',1200,'2018-02-14','0000-00-00 00:00:00',1),(53,1,'Daniel Agostini','2017-08-01',200,'2018-02-12','0000-00-00 00:00:00',1),(54,1,'Daniel Agostini','2017-09-01',200,'2018-02-12','0000-00-00 00:00:00',1),(55,1,'Daniel Agostini','2017-10-01',200,'2018-02-12','0000-00-00 00:00:00',1),(56,1,'Daniel Agostini','2017-11-01',200,'2018-02-12','0000-00-00 00:00:00',1),(57,1,'Susana Grillo','2017-09-01',450,'2018-02-27','0000-00-00 00:00:00',1),(58,1,'Susana Grillo','2017-10-01',450,'2018-02-27','0000-00-00 00:00:00',1),(59,1,'Susana Grillo','2017-11-01',450,'2018-02-27','0000-00-00 00:00:00',1),(60,1,'Liliana ambrosio','2013-01-01',160,'2018-01-16','0000-00-00 00:00:00',1),(61,1,'Osvaldo Sosa','2017-05-01',580,'2017-12-20','0000-00-00 00:00:00',1),(62,1,'Osvaldo Sosa','2017-06-01',580,'2017-12-20','0000-00-00 00:00:00',1),(63,1,'Osvaldo Sosa','2017-07-01',580,'2017-12-20','0000-00-00 00:00:00',1),(64,1,'Osvaldo Sosa','2017-08-01',580,'2017-12-20','0000-00-00 00:00:00',1),(65,1,'Osvaldo Sosa','2017-09-01',340,'2018-02-06','0000-00-00 00:00:00',1),(67,1,'Osvaldo Sosa','2017-10-01',480,'2018-02-06','0000-00-00 00:00:00',1),(68,1,'Osvaldo Sosa','2017-11-01',480,'2018-02-06','0000-00-00 00:00:00',1),(69,1,'Tamara Ferrer','2017-08-01',400,'2018-02-01','0000-00-00 00:00:00',1),(70,1,'Tamara Ferrer','2017-09-01',400,'2018-02-01','0000-00-00 00:00:00',1),(71,1,'Tamara Ferrer','2017-10-01',400,'2018-02-01','0000-00-00 00:00:00',1),(72,1,'Tamara Ferrer','2017-11-01',400,'2018-02-01','2018-02-12 23:34:36',2),(73,1,'Tamara Ferrer','2017-12-01',400,'2018-02-01','2018-02-12 23:34:36',2),(74,1,'Tamara Ferrer','2018-01-01',400,'2018-02-01','2018-02-12 23:34:36',2),(75,1,'Tamara Ferrer','2018-02-01',400,'2018-02-01','2018-02-12 23:34:36',3),(76,1,'Fernando Contreras','2016-12-01',350,'2017-02-01','2018-02-13 14:55:14',2),(77,1,'Fernando Contreras','2017-01-01',350,'2017-02-01','2018-02-13 14:55:14',3),(78,1,'Fernando Contreras','2017-02-01',350,'2017-02-01','2018-02-12 23:31:06',3),(79,1,'Fernando Contreras','2017-09-01',450,'2018-02-01','0000-00-00 00:00:00',1),(80,1,'Fernando Contreras','2017-10-01',450,'2018-02-01','0000-00-00 00:00:00',1),(81,1,'Fernando Contreras','2017-11-01',450,'2018-02-01','0000-00-00 00:00:00',1),(82,1,'Fernando Contreras','2017-12-01',450,'2018-02-01','2018-02-13 03:37:57',3),(83,1,'Fernando Contreras','2018-01-01',450,'2018-02-01','2018-02-13 03:37:57',3),(84,2,'Julio Aquino','2018-02-01',600,'2018-02-07','0000-00-00 00:00:00',1),(85,2,'Santiago González Rojo','2018-02-01',500,'2018-02-07','0000-00-00 00:00:00',1),(86,2,'Santiago González Rojo','2018-01-01',500,'2018-01-04','0000-00-00 00:00:00',1),(87,2,'Norma Coronel','2017-02-01',1400,'2017-08-08','2018-02-13 00:00:35',2),(88,2,'Norma Coronel','2017-03-01',1400,'2017-08-08','2018-02-13 00:00:35',2),(89,2,'Norma Coronel','2017-04-01',1400,'2017-08-08','0000-00-00 00:00:00',1),(90,2,'Norma Coronel','2017-05-01',1400,'2017-08-08','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `contributions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `role` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'gonzalezrojosantiago@gmail.com','$2y$10$IIXsf5hr2w3Ryi1GdxFjneklANBYWUzkaMco2V9HOur9cYWjFoEPe','Santiago','Rojo',1),(2,'carlosdamian.munoz@gmail.com','$2y$10$cwDMD2hTUaXcoMBNXQLF8.6uNzaUsSvJ8r6D61kzbbWHXtPDgTzYW','Carlos Damián',' Muñoz',1),(3,'carinafichera@gmail.com','$2y$10$xUxAKCWdqyZqpuztpvUfkOMz9s0bFBPauFty3LBuEUiK6HLnhBXkK','Carina','Fichera',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-18 13:58:53
